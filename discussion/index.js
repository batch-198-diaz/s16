// index.js

// Arithmetic Operators

let x = 1397;
let y = 7831;

let sum = x + y;
let difference = y - x;
let product = x * y;
let quotient = x / y;

console.log("Addition operator: " + sum);
console.log("Subtraction operator: " + difference);
console.log("Multiplication operator: " + product);
console.log("Division operator: " + quotient);

let remainder = y % x;
console.log("Modulo operator: " + remainder);


// Assignment Operators ex. (=)

let assignment_number = 8;
assignment_number += 2;
console.log("Addition assignment: " + assignment_number);
	// result (8+2): 10

let string1 = "Boston";
let string2 = "Celtics";
string1 += string2;
console.log(string1);
	//result: BostonCeltics (string1 = string1 + string2)

assignment_number += 2;
console.log("Addition assignment: " + assignment_number); // result: 12
assignment_number -= 2;
console.log("Subtraction assignment: " + assignment_number); // result: 10
assignment_number *= 2;
console.log("Multiplication assignment: " + assignment_number); // result: 20
assignment_number /= 2;
console.log("Division assignment: " + assignment_number); // result: 10


// Multiple Operators and Parentheses

let mdas = 1 + 2 - 3 * 4 / 5
console.log("MDAS: " + mdas);

	/* result
		3*4 = 12
		12/5 = 2.4
		1+2 = 3
		3-2.4 = 0.6
	*/

let pemdas = 6 / 2 * (1 + 2);
console.log(pemdas); // uses mobile calc bodmas/pemdas


// Increment / Decrement

console.log("======Increment / Decrement======");

let z = 1;

let increment = ++z;
	console.log("Pre-increment: " + increment); // 2
	console.log("Value of z: " + z); // 2

increment = z++;
	console.log("Post-incrememt: " + increment); // 2
	console.log("Value of z: " + z); // 3

let decrement = --z;
	console.log("Pre-decrement: " + decrement); // 2
	console.log("Value of z: " + z); // 2

decrement = z--;
	console.log("Post-decrement: " + decrement); // 2
	console.log("Value of z: " + z); // 1


// Type Coercion

console.log("======Type Coercion======");
let numA = "10";
let numB = 12;

let coercion = numA + numB;
console.log(coercion); //1012
console.log(typeof coercion); //string

let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion); //30
console.log(typeof nonCoercion); //number

let numE = true + 1;
console.log(numE); // result (1+1): 2


// Equality Operator (==) for comparison

console.log("Equality Operator (==)");
console.log(1 == 1); // true
console.log(1 == 2); // false
console.log(1 == "1"); //true
console.log("yes" == "yes"); //true
console.log("yes" == "Yes"); //false
console.log(false == 0); //true

// Inequality Operator (!=)

console.log("Inequality Operator (!=)");
console.log(1 != 1); //false
console.log(1 != 2); //true
console.log(1 != "1"); //false
console.log(0 != false); //false
console.log("johnny" != "johnny"); //false
console.log("johnny" != "Johnny"); //true


// Strict Equality Operator (===)

console.log("Strict Equality Operator (===)");
console.log(1 === 1); //true
console.log(1 === '1'); //false
console.log('johnny' === 'johnny'); //true

let johnny = 'johnny';
console.log('johnny' === johnny); //true
console.log(false === 0); //false

// Strict Inequality Operator (!==)

console.log("Strict Inequality Operator (!==)");
console.log(1 !== 1); //false
console.log(1 !== 2); //true
console.log(1 !== "1"); //true
console.log('johnny' !== 'johnny'); //false


// Relational Operator

let a = 50;
let b = 65;

console.log("======Relational Operator======");

	// GT or greater than (>)
	let isGreaterThan = a > b;
	console.log(isGreaterThan); //false

	// LT or less than (<)
	let isLessThan = a < b;
	console.log(isLessThan); //true

	// GTE or greater than or equal (>=)
	let isGTE = a >= b;
	console.log(isGTE); //false

	// LTE or less than or equal (<=)
	let isLTE = a <=b;
	console.log(isLTE); //true

let numStr = '30';
console.log(a > numStr); //true
console.log('ye' < 'no');

// AND Operator (&&)

let isAdmin = false;
let isRegistered = true;
let isLegalAge = true;

console.log("======AND Operator (&&)======")

let authorization1 = isAdmin && isRegistered;
	console.log(authorization1); //false
let authorization2 = isLegalAge && isRegistered;
	console.log(authorization2); //true
let authorization3 = isAdmin && isLegalAge;
	console.log(authorization3); //false
let random = isAdmin && false;
	console.log(random); //false


let requiredLevel = 95;
let requiredAge = 18;

let authorization4 = isRegistered && requiredLevel === 25;
	console.log(authorization4);	//false
let authorization5 = isRegistered && isLegalAge && requiredLevel === 95;
	console.log(authorization5);	//true


let username = 'gamer2022';
let username2 = 'theTinker';
let userAge = 15;
let userAge2 = 26;

let registration1 = username.length > 8 && userAge >= requiredAge;
console.log(registration1); //false
let registration2 = username2.length > 8 && userAge2 >= requiredAge;
console.log(registration2); //true

// OR Operator (|| - Double Pipe)

console.log("OR Operator (|| - Double Pipe)")

let userLevel = 100;
let userLevel_2 = 65

let guildRequirement = isRegistered && userLevel >= requiredLevel && userAge >= requiredAge;
console.log(guildRequirement); //false

let guildRequirement_2 = isRegistered || userLevel_2 >= requiredLevel || userAge2 >= requiredAge;
console.log(guildRequirement_2); //true

let guildRequirement_3 = userLevel >= requiredLevel || userAge >= requiredAge;
console.log(guildRequirement_3); //true

let guildAdmin = isAdmin || userLevel_2 >= requiredLevel;
console.log(guildAdmin); //false

// NOT Operator (!)

console.log("======NOT Operator (!)======")

console.log(!isRegistered); //false

let opposite1 = !isAdmin;
console.log(opposite1); //true